#!/bin/sh

#
# Choose CMSSW version
#
cmsswvsn=CMSSW_2_2_11
if [ "X$1" != "X" ]
then
   cmsswvsn=$1
fi
#
# Print CMS_PATH
#
printf "CMS_PATH $CMS_PATH\n"
#
# Check that scramv1 command was defined by . $CMS_PATH/cmsset_default.sh
#
type scramv1 > /dev/null 2>&1
sset=$?
if [ $sset -ne 0 ]
then 
   printf "AnalysisTestUser.sh: Error. scramv1 not found\n"
   exit $SAME_ERROR
fi
#
# Get Working Directory
#
current=`pwd`
#
# Set up CMSSW 
#

tmpfile=`mktemp /tmp/tmp.scram.XXXXXXXXXX`
printf "scramv1 project CMSSW $cmsswvsn ... starting\n"
scramv1 project CMSSW $cmsswvsn > $tmpfile 2>&1
scms=$?
if [ $scms -ne 0 ]
then
   cat $tmpfile
   printf "AnalysisTestUser.sh: Error. $cmsswvsn not available\n"
   exit $SAME_ERROR
fi
tail -5 $tmpfile
printf "scramv1 project CMSSW $cmsswvsn ... completed\n"
#
cd $cmsswvsn/src
export SCRAM_ARCH=`scramv1 arch`
printf "scramv1 runtime -sh ... starting\n"
eval `scramv1 runtime -sh`
printf "scramv1 runtime -sh ... completed\n"
#
# Return to working directory 
#
cd $current
#
# Create configuration file for cmsRun using example from CMS workbook tutorial
#

/bin/cat > analysis_test.py <<EOI
import FWCore.ParameterSet.Config as cms

process = cms.Process("CopyJob")

process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.threshold = 'INFO'
process.MessageLogger.cerr.FwkReport.reportEvery = 1

process.source = cms.Source("PoolSource",
     fileNames = cms.untracked.vstring("/store/user/test/TenEvents.root")
)

process.out = cms.OutputModule("PoolOutputModule",
     fileName = cms.untracked.string('myout.root'),
     fastCloning = cms.untracked.bool(False)
)
process.e = cms.EndPath(process.out)
EOI

echo ""
echo "================= cmsRun configuration file: analysis_test.py ====================="
echo ""
cat analysis_test.py
echo ""
start=`date +%s`
#
# Run cmsRun
#
printf "local file Analysis test\n"
printf "START TIME: `date` --> running cmsRun -j fjr.xml -p analysis_test.py\n"
cmsRun -j fjr.xml -p analysis_test.py
srun=$?
echo ""
echo "================= trimmed framework job report file: fjr.xml ====================="
echo ""
cat fjr.xml | grep -v Branch | grep -v LumiSection | grep -v RunNumber | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g'
echo "=================================================================================="
echo ""

printf "`date` --> DONE\n"
stop=`date +%s`
printf "ELAPSED TIME: $[ $stop - $start ] seconds\n"
#
# Check FJR for errors
#
grep -q "FrameworkError " fjr.xml
sfjr=$?
if [ $sfjr -eq 0 ]
then
   printf "AnalysisTestUser.sh: FrameworkError found in Framework Job Report\n"
   grep "FrameworkError " fjr.xml | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g'
   # if possible extract error
   which xml_grep > /dev/null 2>1 && xml_grep FrameworkError fjr.xml
fi

#
# Check cmsRun exit status
#
if [ $srun -ne 0 -o $sfjr -eq 0 ]
then
   printf "AnalysisTestUser.sh: Error $srun from cmsRun\n"
   exit $SAME_ERROR
fi

#
# Exit
#
printf "OK\n"
exit $SAME_OK
